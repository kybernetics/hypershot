# pylint: disable=wildcard-import, unused-wildcard-import, unused-import, missing-docstring
# pylint: disable=invalid-name, protected-access
"""Test 'mediainfo' module."""
import os

import pytest

from hypershot.mediainfo import *

IMAGE_FILE = os.path.join(os.path.dirname(__file__), '../../docs/_static/hypershot-logo-150.png')

MI_PNG = """
General
Complete name                            : foobar.png
Format                                   : PNG
Format/Info                              : Portable Network Graphic
File size                                : 23.9 KiB

Image
Format                                   : LZ77
Width                                    : 156 pixels
Height                                   : 89 pixels
Bit depth                                : 24 bits
"""

MI_MKV = """
General
Unique ID                                : 000000000000000000000000000000000000000 (0x11111111111111111111111111111111)
Complete name                            : foobar.mkv
Format                                   : Matroska
Format version                           : Version 4 / Version 2
File size                                : 987 MiB
Duration                                 : 12mn 34s
Overall bit rate mode                    : Variable
Overall bit rate                         : 1 234 Kbps
Encoded date                             : UTC 2017-01-23 12:34:56
_STATISTICS_TAGS                         : BPS DURATION NUMBER_OF_FRAMES NUMBER_OF_BYTES

Video
ID                                       : 1
Format                                   : AVC
Format/Info                              : Advanced Video Codec
Format profile                           : Main@L4.0
Format settings, CABAC                   : Yes

Audio
ID                                       : 2
Format                                   : AC-3
Format/Info                              : Audio Coding 3
Bit rate mode                            : Constant
Bit rate                                 : 384 Kbps

Text #1
ID                                       : 3
Format                                   : UTF-8
Codec ID                                 : S_TEXT/UTF8
Codec ID/Info                            : UTF-8 Plain Text
Title                                    : English
Language                                 : English
Default                                  : No
Forced                                   : No

Text #2
ID                                       : 4
Format                                   : UTF-8
Codec ID                                 : S_TEXT/UTF8
Codec ID/Info                            : UTF-8 Plain Text
Title                                    : Spanish
Language                                 : Spanish
Default                                  : No
Forced                                   : No

Text #3
ID                                       : 5
Format                                   : UTF-8
Codec ID                                 : S_TEXT/UTF8
Codec ID/Info                            : UTF-8 Plain Text
Title                                    : French
Language                                 : French
Default                                  : No
Forced                                   : No
"""


def test_parse_mediainfo_for_png():
    info = parse_mediainfo(MI_PNG)
    general, image = info.general[0], info.image[0]

    assert 'video' in info
    assert 'audio' in info
    assert 'text' in info
    assert 'image' in info

    assert len(info.general) == 1
    assert len(info.image) == 1
    assert not info.video
    assert not info.audio
    assert not info.text

    assert general.complete_name == 'foobar.png'
    assert general.format == 'PNG'
    assert general.file_size == '23.9 KiB'

    assert image.format == 'LZ77'
    assert image.bit_depth == '24 bits'


def test_parse_mediainfo_for_mkv():
    info = parse_mediainfo(MI_MKV)

    assert info.general[0]._statistics_tags == 'BPS DURATION NUMBER_OF_FRAMES NUMBER_OF_BYTES'
    assert info.video[0].format_info == 'Advanced Video Codec'
    assert info.audio[0].format_info == 'Audio Coding 3'
    assert len(info.text) == 3

    sub_langs = ['English', 'Spanish', 'French']
    assert all(x.format == 'UTF-8' for x in info.text)
    assert [x.title for x in info.text] == sub_langs
    assert [x.language for x in info.text] == sub_langs


def test_mediainfo_with_basename_from_image_file():
    info = mediainfo(IMAGE_FILE)
    assert info.general[0].complete_name == os.path.basename(IMAGE_FILE)


def test_mediainfo_with_full_path_from_image_file():
    info = mediainfo(IMAGE_FILE, filename_only=False)
    assert info.general[0].complete_name == IMAGE_FILE
