# pylint: disable=invalid-name
""" Create screen shots of a video file, and upload them to an image host.

    | Copyright (c) 2017 Kybernetics Project · MIT licensed
"""

__version__ = '0.4.1.dev1'
section = 'kybernetics'

version_info = tuple(int(_) if _.isdigit() else _ for _ in __version__.split('.'))
