""" Hypershot Mediainfo Handling.
"""
import os
import subprocess

from addict import Dict as attrdict

from . import util


def parse_mediainfo(text):
    """Parse mediainfo output into structured data."""
    # Always add empty standard sections
    result = attrdict(general=[], video=[], audio=[], text=[])

    section = None
    for line in text.splitlines():
        line = line.strip()
        if not line:
            continue

        if ':' in line:
            if section:
                key, val = line.split(':', 1)
                result[section][-1].setdefault(util.make_identifier(key.strip()), val.strip())
        else:
            section = line.lower()
            if '#' in section:
                # Just keep the name of numbered sections (they get appended to their list)
                section = section.split()[0]
            result.setdefault(section, [])  # add possible non-standard section
            result[section].append(attrdict())  # append new namespace for values that follow

    return result


def mediainfo(filepath, filename_only=True, encoding='utf-8'):
    """Return parsed mediainfo for a given file."""
    filename, workdir = filepath, None
    if filename_only:
        filename, workdir = os.path.basename(filepath), os.path.dirname(filepath)

    text = subprocess.check_output(['mediainfo', filename], cwd=workdir).decode(encoding)
    return parse_mediainfo(text)
