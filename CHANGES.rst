=========
Changelog
=========

* :feature:`0` TODO: fetching custom metadata (IMDb, TVMaze, etc)
* :feature:`0` TODO: do direct image rehosting on supporting services
* :feature:`0` TODO: caching logic
* :feature:`0` TODO: create thumbnail on demand
* :feature:`0` TODO: paste to clipboard
* :feature:`0` TODO: screen shot logic
* :feature:`0` TODO: add shell scripts

* :support:`0` Main documentation now on *Read the Docs*
* :feature:`0` ``upload`` now takes URLs for image rehosting

* :release:`0.3.1 <2017-06-18>`
* :feature:`0` Detailed progress bar display for uploads
* :support:`1` Publish docs / changelog to *Read the Docs*.
* :support:`0` Publish coverage reports to *GitLab Pages*.
* :support:`0` Added most of the missing unit tests.
* :support:`0` Create development virtualenv using ``tox``.

* :release:`0.2.1 <2017-06-16>`
* :feature:`0` Services can be switched off using ``enabled: no`` in the config
* :feature:`0` Pretty table output for ``services`` and ``templates``.
* :feature:`0` New ``--no-progress`` option.
* :feature:`0` New ``--quiet`` option.
* :feature:`0` New ``templates`` sub-command.
* :feature:`0` Added Jinja2 templating for upload results.

* :release:`0.1.1 <2017-06-14>`
* :feature:`0` Upload to *imgur*, any *Chevereto* image host, and ‘simple file upload’ ones.
* :feature:`0` Basic command line, logging and configuration handling.
