Installation
============

Installing the Python Application
---------------------------------

You can install this software into your user home by using the following
commands:

.. code-block:: bash

    mkdir -p ~/.local/venvs/hypershot && python3 -m venv $_ ; . $_/bin/activate
    pip install -U pip
    pip install -r "https://gitlab.com/kybernetics/hypershot/raw/master/requirements.txt"
    pip install hypershot

    mkdir -p ~/bin && ln -nfs ../.local/venvs/hypershot/bin/hypershot $_

Doing it this way ensures that the software is installed in isolation
not interfering with other apps, and vice versa. It also makes
uninstalling very easy, because all files are contained in a single
directory tree.

For a global install, do the above as ``root`` and replace ``~/.local``
by ``/usr/local``, and also replace the last command by this:

.. code-block:: bash

    ln -nfs ../venvs/hypershot/bin/hypershot /usr/local/bin

You might need to install ``pyvenv`` first, on Debian and Ubuntu this is
done using ``sudo apt-get install python3-venv``. If your platform does
not come with a suitable Python3 package, consider using `pyenv`_ to get
Python 3.4+.


Installing Tools
----------------

For uploading images, the above installation is all you need. Doing
screen shots though requires some additional tools to be available on
the system you want to use ``hypershot`` on. You need to provide
``mediainfo`` and at least one of the supported video players. The
following describes installation on *Debian* and derived distros, for
others call the native package manager instead of APT.

``mediainfo`` and ``mplayer`` come pre-packaged starting with *Debian
Wheezy* and *Ubuntu Trusty*, so just install them like this:

.. code-block:: bash

    apt-get install mediainfo mplayer2

**TODO** Is ``mpv`` the better option?

The shell script bundled with this software is able to also use
``ffmpeg`` or ``avconv``. Depending on the type of video file, these
might be able to handle cases ``mplayer`` can not, and vice versa, so
it's best to have both.

Especially when it comes to *HEVC* encoded media (also known as
*x.265*), it's best to have a current *ffmpeg* version. You can get one
from the project's `FFmpeg Static Builds`_ website, which also avoids
the problems resulting from the *“switch between ffmpeg and avconv and
back”* game Linux distros played a while ago.

To install the git build of ``ffmpeg``, do this:

.. code-block:: bash

    mkdir -p ~/.local/ffmpeg && cd $_
    wget "https://johnvansickle.com/ffmpeg/builds/ffmpeg-git-64bit-static.tar.xz"
    tar -xJ --no-same-owner --strip-components=1 -f $(basename "$_")
    ln -nfs $PWD/{ffmpeg,ffmpeg-10bit,ffprobe} ~/bin

Choose another archive from the website for 32bit or ARM machines
(*Raspberry Pi*).

If you're able to become ``root``, install into ``/opt/ffmpeg`` and
create the symlinks in ``/usr/local/bin`` instead.


Next you need to provide some :doc:`setup` before you can use the software.


.. _pyenv: https://github.com/pyenv/pyenv
.. _FFmpeg Static Builds: https://www.johnvansickle.com/ffmpeg/
