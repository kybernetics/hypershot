Usage
=====

The ``hypershot`` command line tool creates screen shots of a video file,
and uploads them to an image host.
You can also upload existing images,
using the same configuration as for the screen shots.

.. figure:: https://gitlab.com/kybernetics/hypershot/raw/master/docs/_static/hypershot-demo.gif
   :align: center
   :alt: Demo Terminal Session

   Demo Terminal Session


Common Options
--------------

Look at the start of the `cli.py`_ module for the most up-to-date usage information
on the ``hypershot`` command, or call ``hypershot -h`` after installation.

Here is a copy of the ``--help`` output, but it might be outdated:

.. code-block:: console

    $ hypershot --help
    Create screen shots of a video file, and upload them to an image host.

    Usage:
        hypershot [options] upload <image>...
        hypershot [options] services
        hypershot [options] templates
        hypershot [options] <video>...
        hypershot (-h | --help)
        hypershot --version

    Options:
        -h, --help          Show this screen.
        --version           Show version.
        --debug             Enable debugging features?
        -q, --quiet         Hide result output?
        -v, --verbose       Verbose logging?
        -n, --dry-run       Do not really upload images

        -c PATH, --config-dir=PATH
            Custom configuration directory.

        -s NAME, --service=NAME
            Select image hosting service.

        -t NAME, --template=NAME
            Select template for result formatting.

        -T PIXELS, --thumb-size=PIXELS
            Also create thumbnail with given width.

See the :doc:`setup` chapter for examples and details on all
supported configuration settings. Without a configuration file, only
``imgur`` is available for uploading, and you still need to provide
access credentials for that.
So be sure to read :ref:`this section <imgur>` about it.

``--help`` and ``--version`` print usage and version information, as
usual.

``--debug`` activates detailed logging (level ``DEBUG``), and in case of
errors also prints full *Python* tracebacks where normally a simple
one-line message is shown.

``--quiet`` hides any result output, so that only the clipboard is
filled.

``--verbose`` raises the logging level from the default ``WARNING`` to
``INFO``. It also enables progress indicators, whatever the
configuration says, unless combined with ``--no-progress``.

``--dry-run`` prevents image uploading and writing to disk – although
screen shots are written to ``/tmp`` anyway.


Generating and Uploading Screen Shots
-------------------------------------

If you feed ``hypershot`` with a list of video files, they're first
inspected using ``mediainfo``. Then the requested number of screen shots
is taken, evenly spaced over the video's duration but starting with a
small offset. If only one is requested, it is taken from smack in the
middle.

After uploading the generated images, the resulting links are fed into a
*Jinja2* template. To list all available templates, call
``hypershot templates``.

.. figure:: https://gitlab.com/kybernetics/hypershot/raw/master/docs/_static/hypershot-templates.png
   :align: center
   :alt: Output of 'hypershot templates'

   Output of 'hypershot templates'
Use ``--template`` to select one from the list, or set a default in your
configuration. For more details, see :doc:`templating`.


.. upload:

Uploading Existing Images
-------------------------

Besides taking screenshots, you can also upload existing image files
explicitly, via the ``upload`` sub-command. The image links are also fed
into the result template, but no videofile or mediainfo values are
available (the ``videos`` value is empty and thus logically ``False``,
so you can check on that in a template).

Rehosting is also possible, by passing ``https``, ``http`` or ``ftp`` URLs.


.. _cli.py: https://gitlab.com/kybernetics/hypershot/blob/master/src/hypershot/cli.py
