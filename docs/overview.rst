Introduction
============

What it Does
------------

Look at one or more video files, taking screen shots without any human
interaction, uploading the results to an image hosting service, and
finally produce some text output containing links to the images. That
output can be used for posting to forums, blogs, etc.

You can also upload existing images, using the same configuration as for
the screen shots.

*hypershot* is designed for and tested on *Linux*, and it is expected
and supported to run on *Mac OSX* (report any issues you might
encounter). It *might* run on *Windows*, if you use *CygWin/Babun*,
*Ubuntu for Windows*, or one of the *Docker* distributions for
*Windows*.


How it Works
------------

*hypershot* looks at a video file using *mediainfo*, and then decides on
the offsets for the screen shots, depending on how many you requested
and the duration of the video. It then calls an external script or
command to take those screenshots – a default script using *mplayer*,
*ffmpeg* or *avconv* is provided.

The resulting images are then uploaded to a configured image hoster, and
the returned URLs plus the mediainfo data are fed into a templating
engine. This way you can generate HTML, BBcode, Markdown, or whatever
(text) format you need. Then take the final result and post your screen
shots on the web – for your convenience, it's already in your paste
buffer.

See :doc:`usage` for more details, and the :doc:`following chapter <install>`
on how to install the necessary software.
