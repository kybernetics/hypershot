Configuration
=============

If just want to get started quickly, drop the `example configuration files`_
into the ``~/.config/hypershot`` directory, as follows:

.. code-block:: shell

    mkdir -p ~/.config/hypershot && cd $_
    curl -sL "https://gitlab.com/kybernetics/hypershot/repository/archive.tar.gz?ref=master" \
        | tar -xvz --strip-components=3 --wildcards '*/docs/examples/*.yaml'

Then follow the :ref:`imgur` link, if you want to use that service.
Otherwise skip to the :doc:`usage` chapter.


Configuration File
------------------

Configuration is read from the file ``~/.config/hypershot/config.yaml``
(on Linux, following the XDG spec). Only `YAML`_ is supported. You can
set a different location for the configuration file using
``--config-dir``, the file itself is always called either
``config.yaml`` or ``config.yml``.

All command line parameters can be given a custom default via either the
configutation file or an environment variable. Specifically,
``HYPERSHOT_CONFIG_DIR`` can be used to set a different default for the
``--config-dir`` option.

The lookup order is this (first wins): command line options, environment
variables, configuration settings, configuration defaults.

To select a named image hosting service (which can be configured as
shown in the next section), use either ``service: ‹name›`` in the config
file, ``HYPERSHOT_SERVICE=‹name›`` in the environment, or ``-s ‹name›``
on the command line.

*Any* option that takes a value works this way.


Image Hosters
-------------

To list all the image hosting services, both provided as defaults and
those added via the configuration, call ``hypershot services``.

Below you find information on how to configure the built-in ``imgur``
service, and how to add others to your configuration.

Also see `this config.yaml`_ for more examples,
using it provides you with the following services:

.. code-block:: console

    $ hypershot -c docs/examples services
    ››› Services ‹‹‹
    Name   Handler       Max. Size  Ext  Service URL
    imgbb  chevereto      16.0 MiB    1  https://imgbb.com
    imgur  imgur          10.0 MiB    2  https://api.imgur.com
    lutim  file_upload     5.0 MiB    1  https://lut.im
    malzo  chevereto      30.0 MiB    1  https://malzo.com

    ››› 'Ext'ensions column legend ‹‹‹
    Ext  Image File Extensions
      1  BMP GIF JPEG JPG PNG
      2  APNG GIF JPEG JPG PDF PNG TIFF XCF


.. _imgur:

imgur
"""""

To use the built-in ``imgur`` service you need to `register`_ with them.
Select *“Anonymous usage without user authorization”*, which will give
you a *client ID* and a *client secret*.

Add those values to the ``~/.netrc`` file like this:

.. code-block:: cfg

    machine hypershot:api.imgur.com
        login ‹CLIENT_ID›
        password ‹CLIENT_SECRET›


Simple File Upload Sites
""""""""""""""""""""""""

If a site basically does a HTML form upload (``multipart/form-data``),
use the ``file_upload`` handler.

Consider this example for `https://lut.im/`_:

.. code-block:: yaml

    services:
      lutim:
        handler: file_upload
        url: "https://lut.im/"
        limit: 5M
        types: [JPG, PNG, BMP, GIF]
        upload_url: "{url}"
        headers:
          Referer: "{url}"
        data:
          delete-day: 0
          crypt: on
        files_field_name: "file"
        response_regex: "<a href=.(?P<scheme>[^:]+)://(?P<domain>[^/]+)/\
                         (?P<image>[^\"]+).[^>]*><img class=.thumbnail"
        image_url: "https://{response[domain]}/{response[image]}"

You can set the HTTP POST request ``headers``, and add any form ``data``
in addition to the file upload field. The name of that field must be
given in ``files_field_name``.

The provided ``response_regex`` is used to scan a HTTP response of type
``text/html`` or ``text/plain``, and must contain at least one named
group of the form ``(?P<name>...)``
– see Python's `Regular Expression HOWTO`_ for more details.
Those named groups are available in
``response``, in addition to all the handler's settings, to build an
``image_url`` using the `Python string formatter`_.

In case of a JSON response, you can use ``json`` instead of ``response``
for building your ``image_url``.


Chevereto Sites
"""""""""""""""

A good service powered by `Chevereto`_ is ``malzo.com``, because you can
use it anonymously and it has a high size limit of 30 MiB. If you want
to use an account you have there, the next paragraph shows you how –
otherwise leave out the ``login`` attribute.

Here is an example including user account credentials – these settings
go into ``config.yaml`` like all other ones:

.. code-block:: yaml

    services:
      malzo:
        handler: chevereto
        url: "https://malzo.com"
        limit: 30M
        types: [JPG, PNG, BMP, GIF]
        login: .netrc

In this example, the special value ``.netrc`` means the username and
password are kept separate in the ``~/.netrc`` file, which is commonly
used to store credentials for FTP access and similar services.
Otherwise, provide ``login`` and ``password`` in the YAML file directly.

So also add this to the ``~/.netrc`` file:

.. code-block:: cfg

    machine hypershot:malzo.com
        login YOURNAME
        password KEEP_ME_SECRET

This file must be private, therefor call ``chmod 0600 ~/.netrc`` after
you initially create it.


Logging Configuration
---------------------

The Python logging system can be configured by one of the files
``logging.yaml``, ``logging.yml``, or ``logging.ini``. They must be
located in the configuration directory, and are checked in the mentioned
order.

Consult the `Python Guide`_ and the `Logging How-To`_ for details on the
logging machinery and its configuration. For the YAML files, the
*dictionary* method applies (using ``dictConfig``), see `this
logging.yaml`_ for a full example.

The logging level threshold of the root logger depends on the values of
``debug`` (``DEBUG``) and ``verbose`` (``INFO``) – if neither is set,
the level is ``WARNING``.


.. _YAML: http://lzone.de/cheat-sheet/YAML
.. _example configuration files: https://gitlab.com/kybernetics/hypershot/blob/master/docs/examples/
.. _this config.yaml: https://gitlab.com/kybernetics/hypershot/blob/master/docs/examples/config.yaml
.. _register: https://api.imgur.com/oauth2/addclient
.. _`https://lut.im/`: https://lut.im/
.. _Python string formatter: https://pyformat.info/#getitem_and_getattr
.. _Chevereto: https://chevereto.com/
.. _Python Guide: http://python-guide-pt-br.readthedocs.io/en/latest/writing/logging/#logging-in-an-application
.. _Logging How-To: https://docs.python.org/3/howto/logging.html#basic-logging-tutorial
.. _this logging.yaml: https://gitlab.com/kybernetics/hypershot/blob/master/docs/examples/logging.yaml
.. _Regular Expression HOWTO: https://docs.python.org/3.4/howto/regex.html#non-capturing-and-named-groups
