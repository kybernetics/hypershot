Templating of Upload Results
============================

Templating Introduction
-----------------------

As mentioned earlier, after uploading image files, the resulting links
are fed into a *Jinja2* template.

To list all the currently defined templates, use the
``hypershot templates`` command.

Template file names always end with a ``.j2`` extension. Built-in ones
can be overridden by placing a file with the same name in the
configuration directory.

You can also pass the path of an existing file with the ``-t`` option,
then the template can be located anywhere.

If you want to write your own templates, or have some error in an
existing one, the ``yaml`` and ``json`` templates come in handy. They
dump all the keys and values available to a template as a YAML or JSON
document, so you can see what's where.


Writing Custom Templates
------------------------

See the `Jinja homepage`_ for the full documentation of the template
engine. You might not need it at all, if the available built-in
templates serve your needs – then just skip this section.

Here's the built-in ``bbcode.j2`` template as an example:

.. code-block:: jinja

    {# Simple BBcode Template [built-in] #}
    {%- for image in images -%}
    [img]{{ image.hypershot.link }}[/img]
    {% endfor %}

Start your own templates with a comment line like shown, so they get a
proper description in ``hypershot templates``.

These additional utility functions are available:

-  ``u.context`` – The Jinja2 context object, make sure you understand
   what you're doing when accessing this.
-  ``u.namespace`` – A shallow copy of the Jinja2 template namespace,
   without helper functions.

And these are the custom filters:

-  ``json`` – Dump the value as JSON document.
-  ``mask_secrets`` – Replace any secret values (passwords) with
   question marks.
-  ``re_sub(pattern, repl)`` – Substitute regular expression matches
   with the text in ``repl``.
-  ``repr`` – Apply Python's ``repr``.
-  ``yaml`` – Dump the value as YAML document.


.. _Jinja homepage: http://jinja.pocoo.org/
