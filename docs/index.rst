.. Hypershot documentation

Welcome to Hypershot's documentation!
=====================================

The ``hypershot`` command line tool creates screen shots of a video file,
and uploads them to an image host.
You can also upload existing images,
using the same configuration as for the screen shots.

.. figure:: https://gitlab.com/kybernetics/hypershot/raw/master/docs/_static/hypershot-demo.gif
   :align: center
   :alt: Demo Terminal Session

   Demo Terminal Session

This manual is separated into these chapters, they should be read in order:

* :doc:`overview` describes what ``hypershot`` can do for you in more detail.
* :doc:`install` shows how the application itself and necessary tools are installed.
* :doc:`setup` explains how you can add image hosting services and other custom settings.
* Read :doc:`usage` to learn about the different ways to call the ``hypershot`` command.
* If you want to add your own result templates, consult :doc:`templating` for details.
* The :doc:`changelog` is a timeline of the project's history, check this after an update
  for new features and other relevant changes.

If you're a developer, see the `GitLab README`_ for some information on working with the source code.

 |GitLab CI| |Coverage Report| |PyPI|


.. warning::

    This project is under development, and not all features described here are implemented yet.
    So do not be surprised if you just get an error message when you try some things.

    Consult the :doc:`changelog` regarding the implementation progress.


.. _GitLab README: https://gitlab.com/kybernetics/hypershot#hypershot

.. |GitLab CI| image:: https://gitlab.com/kybernetics/hypershot/badges/master/build.svg
   :alt: GitLab CI
   :target: https://gitlab.com/kybernetics/hypershot/commits/master

.. |Coverage Report| image:: http://img.shields.io/badge/coverage-%3e80%25-5555ff.svg
   :alt: Coverage Report
   :target: https://kybernetics.gitlab.io/hypershot/

.. |PyPI| image:: https://img.shields.io/pypi/v/hypershot.svg
   :alt: PyPI Release
   :target: https://pypi.python.org/pypi/hypershot/


Detailed Contents
-----------------

.. toctree::
   :maxdepth: 3

   overview
   install
   setup
   usage
   templating
   changelog


Indices & Tables
----------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
