# Hypershot

The ``hypershot`` command line tool creates screen shots of a video file,
and uploads them to an image host.
You can also upload existing images,
using the same configuration as for the screen shots.

![Demo Terminal Session](https://gitlab.com/kybernetics/hypershot/raw/master/docs/_static/hypershot-demo.gif)

You can find the **[main documentation](http://hypershot.readthedocs.io/en/latest/)**
regarding installation, configuration and usage
on *Read the Docs*, what follows is just developer information.

**IN DEVELOPMENT**

[![GitLab CI](https://gitlab.com/kybernetics/hypershot/badges/master/build.svg)](https://gitlab.com/kybernetics/hypershot/commits/master)
[![Coverage Report](http://img.shields.io/badge/coverage-%3e80%25-5555ff.svg)](https://kybernetics.gitlab.io/hypershot/)
[![PyPI](https://img.shields.io/pypi/v/hypershot.svg)](https://pypi.python.org/pypi/hypershot/)
[![Documentation](https://readthedocs.org/projects/hypershot/badge/?version=latest)](http://hypershot.readthedocs.io/en/latest/?badge=latest)

**Contents**

 * [Working with the Source Code](#working-with-the-source-code)
   * [Creating a Working Directory](#creating-a-working-directory)
   * [Building the Documentation Locally](#building-the-documentation-locally)
   * [Releasing to PyPI](#releasing-to-pypi)
 * [Links](#links)


## Working with the Source Code

### Creating a Working Directory

    deactivate 2>/dev/null
    pip3 --version || sudo apt-get install python3-pip
    xargs -n1 pip3 install --user -U <<<"pip tox"

    # Use "git@gitlab.com:kybernetics/hypershot.git" if you have developer access
    git clone "https://gitlab.com/kybernetics/hypershot.git"
    cd hypershot && ~/.local/bin/tox -e $_
    . .env


### Building the Documentation Locally

To build the *Sphinx* documentation, call ``tox -e docs``.
On success, the index page can be found at ``docs/_build/html/index.html``.


### Releasing to PyPI

Building and uploading a (pre-)release:

    # pre-release
    : $(( 1 + $(./setup.py --version | cut -f4 -d. | tr -cd 0-9) ))
    bumpr -s $(./setup.py --version | tr -d .0-9)$_

    # release
    version="$(./setup.py --version)"
    git commit -m "Release $version" setup.cfg

    git tag -a "v$version" -m "Release $version" && git push && git push --tags

    # build & upload
    rm -rf dist ; ./setup.py sdist bdist_wheel
    twine upload --config-file setup.cfg dist/*.{zip,whl}

    # post release
    bumpr -m 2>/dev/null && bumpr -p -s dev1 && git add src/*/__init__.py
    git commit -m "bump to $(./setup.py --version) after $version release"


## Links

 * [Main hypershot documentation](http://hypershot.readthedocs.io/en/latest/)
 * [docopt Manual](http://docopt.org/)
 * [A hands-on introduction to video technology](https://github.com/leandromoreira/digital_video_introduction#intro)
 * ``small.mp4`` sample from [techslides.com](http://techslides.com/sample-webm-ogg-and-mp4-video-files-for-html5)
